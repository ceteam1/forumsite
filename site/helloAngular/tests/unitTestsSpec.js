﻿
// ---SPECS-------------------------

describe('Xtrack', function () {
    var scope,
    controller;
    beforeEach(function () {
        module('Xtrack');
    });

        var $httpBackend, $rootScope, mainController, authRequestHandler;


        beforeEach(inject(function ($injector, $httpBackend) {
            // Set up the mock http service responses
            $httpBackend = $injector.get('$httpBackend');

            // Get hold of a scope (i.e. the root scope)
            $rootScope = $injector.get('$rootScope');

            // The $controller service is used to create instances of controllers
            var $controller = $injector.get('$controller');

            mainController = function () {
                return $controller('mainCtrl', { '$scope': $rootScope });





        $httpBackend.whenRoute('GET', 'forums/:id')
  .respond(function (method, url, data, headers, params) {
      return [200, MockForumList[Number(params.id)]];
  });

        $httpBackend.whenRoute('GET', 'subforums/:id')
.respond(function (method, url, data, headers, params) {
    return [200, MockForumList[Number(params.id)]];
});

        $httpBackend.whenRoute('GET', 'threads/:id')
.respond(function (method, url, data, headers, params) {
    return [200, MockForumList[Number(params.id)]];
});


        $httpBackend.whenRoute('GET', '/forums')
          .respond(function (method, url, data, headers, params) {
              return [200, {
                  content: MockForumList
              }];
          });

        $httpBackend.whenRoute('GET', '/forums')
     .respond(function (method, url, data, headers, params) {
         return [200, {
             content: MockForumList
         }];
     });

        $httpBackend.whenRoute('GET', 'forums/:id/subforums')
     .respond(function (method, url, data, headers, params) {
         return [200, {
             content: MockSubforumList[Number(params.id)]
         }];
     });

        $httpBackend.whenRoute('GET', 'subforums/:id/threads')
     .respond(function (method, url, data, headers, params) {
         return [200, {
             content: MockThreadList[Number(params.id)]
         }];
     });

        $httpBackend.whenRoute('GET', 'subforums/:id/threads')
  .respond(function (method, url, data, headers, params) {
      return [200, {
          content: MockPostList[Number(params.id)]
      }];
  });







        $httpBackend.whenRoute('POST', 'users/login')
.respond(function (method, url, data, headers, params) {
    return [200, {
        content: MockUsersList[data.userid]
    }];
});


        $httpBackend.whenRoute('POST', 'forums')
.respond(function (method, url, data, headers, params) {
    var idV = MockForumsList.length + 1;
    MockForumsList.put({
        id: idV,
        name: data.name,
        summary: data.summery
    })
    return [200, {
        content: MockForumsList[idV]
    }];
});


        $httpBackend.whenRoute('POST', 'subforums/:id/threads')
.respond(function (method, url, data, headers, params) {
    var idV = MockThreadsList.length + 1;
    MockThreadsList.put({
        id: idV,
        name: data.name,
        summary: data.summery
    })
    return [200, {
        content: MockThreadsList[Number(params.id)[idV]]
    }];
});

        $httpBackend.whenRoute('POST', 'threads/:id/posts')
.respond(function (method, url, data, headers, params) {
    var idV = MockPostsList.length + 1;
    MockPostsList.put({
        id: idV,
        name: data.name,
        summary: data.summery
    })
    return [200, {
        content: MockPostsList[Number(params.id)[idV]]
    }];
});

        $httpBackend.whenRoute('POST', 'forums/:id/subforums')
.respond(function (method, url, data, headers, params) {
    var idV = MockSubforumsList.length + 1;
    MockSubforumsList.put({
        id: idV,
        name: data.name,
        summary: data.summery
    })
    return [200, {
        content: MockSubforumsList[Number(params.id)[idV]]
    }];
});

            };
        }));


        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        describe('mainCtrl', function () {
            beforeEach(inject(function ($rootScope, $controller) {
                scope = $rootScope.$new();
                controller = $controller('mainCtrl', {
                    '$scope': scope
                });
            }));

        ////tests

            it('nouser login should fail', function () {
                expect($scope.loggedIn).toBe(false);
                expect($scope.loggedUser).toBe(undefined);
                expect($http.defaults.headers.common.user_token).toBe(undefined);
                expect($http.defaults.headers.common.user_token).toBe('dddd');
                console.log("tests passed")
                $httpBackend.flush();
            });
        });
});



// --- Runner -------------------------
(function () {
    var jasmineEnv = jasmine.getEnv();
    jasmineEnv.updateInterval = 1000;

    var htmlReporter = new jasmine.HtmlReporter();

    jasmineEnv.addReporter(htmlReporter);

    jasmineEnv.specFilter = function (spec) {
        return htmlReporter.specFilter(spec);
    };

    var currentWindowOnload = window.onload;

    window.onload = function () {
        if (currentWindowOnload) {
            currentWindowOnload();
        }
        execJasmine();
    };

    function execJasmine() {
        jasmineEnv.execute();
    }

})();