﻿

var username = element(by.id('userName'));
var password = element(by.id('userPassword'));
var loginButton = element(by.id('login'));

var login = function () {
    element(by.id('userName')).sendKeys('admin');
    element(by.id('userPassword')).sendKeys('admin');
    element(by.id('login')).click();
    //browser.sleep(5000);
}

var logout = function () {
    element(by.id('logout')).click();
    //browser.sleep(5000);
}

describe('allForums', function () {



    describe('allForums', function () {


        beforeEach(function () {
            browser.get('http://localhost:50371/');
        });


        it('login', function () {
            login();
            expect(element(by.id('welcome')).getText()).toEqual('Hello admin');
            logout();
            
        });

        it('login', function () {
            expect(element(by.id('notLength')).getText()).toEqual('0');

        });


        
        it('create New Forum', function () {
            login();
            element(by.id('newForumName')).sendKeys('testNewForumName1');
            element(by.id('newForumSummery')).sendKeys('testNewForumSummery');
            element(by.css('[ng-click="addNewForum()"]')).click();
            browser.waitForAngular();
            expect(element.all(by.repeater('f in forums').column('f.name')).last().getText()).toEqual('testNewForumName1');
            logout();
        });

    });

  
});

describe('forum view', function () {



        beforeEach(function () {
            browser.get('http://localhost:50371/#/forums/1');
        });


        it('login', function () {
            login();
            expect(element(by.id('welcome')).getText()).toEqual('Hello admin');
            logout();
        });


        it('create New Sub Forum', function () {
            login();
            element(by.id('newSubForumName')).sendKeys('testNewSubForumName1');
            element(by.id('newSubForumSummery')).sendKeys('testNewSubForumSummery');
            element(by.css('[ng-click="addNewSubForum()"]')).click();
            browser.waitForAngular();

            expect(element.all(by.repeater('s in subforums').column('s.name')).last().getText()).toEqual('testNewSubForumName1');
            logout();
        });

    });



describe('forum view', function () {
   
        beforeEach(function () {
            browser.get('http://localhost:50371/#/forums/1');
        });


        it('login', function () {
            login();
            expect(element(by.id('welcome')).getText()).toEqual('Hello admin');
            logout();
        });


        it('create New Sub Forum', function () {
            login();
            element(by.id('newSubForumName')).sendKeys('testNewSubForumName1');
            element(by.id('newSubForumSummery')).sendKeys('testNewSubForumSummery');
            element(by.css('[ng-click="addNewSubForum()"]')).click();
            browser.waitForAngular();
            expect(element.all(by.repeater('s in subforums').column('s.name')).last().getText()).toEqual('testNewSubForumName1');
            logout();
        });

    });


    describe('sub forum view', function () {
       


            beforeEach(function () {
                browser.get('http://localhost:50371/#/subforum/1');
            });


            it('login', function () {
                login();
                expect(element(by.id('welcome')).getText()).toEqual('Hello admin');
                logout();
            });


            it('create New Sub Forum', function () {
                login();
                element(by.id('newThreadName')).sendKeys('testThreadName1');
                element(by.id('newThreadSummery')).sendKeys('testThreadSummery');
                element(by.css('[ng-click="createNewThread()"]')).click();
                browser.waitForAngular();
                expect(element.all(by.repeater('t in threads').column('t.name')).last().getText()).toEqual('testThreadName1');
                logout();
            });

        });


   


    describe('thread view', function () {
      

            beforeEach(function () {
                browser.get('http://localhost:50371/#/thread/1');
            });


            it('login', function () {
                login();
                expect(element(by.id('welcome')).getText()).toEqual('Hello admin');
                logout();
            });


            it('create New Post', function () {
                login();
                element(by.id('newPostName')).sendKeys('testPostName1');
                element(by.id('newPostSummery')).sendKeys('testPostSummery');
                element(by.css('[ng-click="addNewPost()"]')).click();
                browser.waitForAngular();
                expect(element.all(by.repeater('p in posts').column('p.name')).last().getText()).toEqual('testPostName1');
                logout();
            });

            it('get notification', function () {
                login();
                element(by.id('newPostName')).sendKeys('testPostName2');
                element(by.id('newPostSummery')).sendKeys('testPostSummery2');
                element(by.css('[ng-click="addNewPost()"]')).click();
                browser.waitForAngular();
                expect(element(by.id('notLength')).getText()).toEqual('2');
                logout();
            });

        });

