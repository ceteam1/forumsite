﻿/// <reference path="C:\Users\jbh\Desktop\14-2-2016\helloAngular\helloAngular\components/angular/angular.js" />
/// <reference path="../models/customerModel.js" />

angular.module("Xtrack").factory("apiSrv", function ($http, $q, $rootScope) {

    var url = "http://132.73.204.242:6140/";
    //var MyCustomers = [];
    var d = $q.defer();
    var httpGet = function (apiUrl, thanFunc) {
        $http({
            method: "get",
            url: url+apiUrl,
            headers: { 'Content-Type': 'application/json'}
        }).then(
                  function (results) {
                      console.log("getting from api " + apiUrl);
                      console.log(results.data);
                      //var forums = JSON.parse(results.data);
                      console.log("data recived..");
                      if (thanFunc != undefined) {
                          thanFunc(results.data);
                      } else {
                          console.log(apiUrl + " than undifined..");
                      } 
                      d.resolve(results.data);
                  },
                  function (err) {
                      console.log(err);
                      d.reject(err);
                  });
        //}
        return d.promise;
    }


    var httpDelete = function (apiUrl, dataV, thanFunc) {

        $http({
            method: 'DELETE',
            url: url + apiUrl,
            data: dataV,
            headers: { 'Content-Type': 'application/json','Access-Control-Allow-Methods':'GET, POST, OPTIONS, PUT, DELETE' }
        }).then(
                 function (results) {
                    
                     d.notify("posting to api " + url + apiUrl + " data" + dataV);
                     console.log("posting to api " + url + apiUrl + " data" + dataV);
                     console.log(results.data);
                     console.log($http.defaults.headers.common);

                     //var res = JSON.parse(results.data.content);
                     d.notify("data recived..");
                     if (thanFunc != undefined) {
                         thanFunc(results.data);
                     }
                     d.resolve(results.data);

                 },
                 function (err) {
                     console.log(err);
                     console.log($http.defaults.headers.common);
                     d.reject(err);
                 });
        //}

        return d.promise;
    }

    var httpPost = function (apiUrl, dataV,thanFunc) {

        $http({
            method: 'POST',
            url: url + apiUrl,
            data: dataV,
            headers: { 'Content-Type': 'application/json'}
        }).then(
                 function (results) {
                     d.notify("posting to api " + url + apiUrl + " data" + dataV);
                     console.log("posting to api " + url + apiUrl + " data" + dataV);
                     console.log(results.data);
                     console.log($http.defaults.headers.common);
                    
                     //var res = JSON.parse(results.data.content);
                     d.notify("data recived..");
                     if (thanFunc != undefined) {
                         thanFunc(results.data);
                     }
                     d.resolve(results.data);

                 },
                 function (err) {
                     console.log(err);
                     console.log($http.defaults.headers.common);
                     d.reject(err);
                 });
        //}
      
        return d.promise;
    }

    return {
        getAllForums: function (then) {
            return httpGet('api/v1/forums', then);
        },

        getAllThreads: function (id,then) {
            return httpGet('api/v1/subforums/' + id + "/threads", then);
        },

        register: function (name, password, email) {
            return httpPost("api/v1/users/register",
                {
                    name: name,
                    password: password,
                    email: email
                });
        },


        setAccessToken: function (token) {
            $rootScope.accessToken = token;
        },

        login: function (nameV, passwordV, forum,then,token) {
            if (forum != "" && nameV != "admin") {
                forum = "/" + forum
            } else {
                forum = ""
            }


            console.log("login: " +forum);
            return httpPost('api/v1/users/login' + forum,
                {
                    name: nameV,
                    password: passwordV
                }, then
                )},


        createNewForum: function (nameV, summeryV) {
            return httpPost('api/v1/forums',
                {
                    name: nameV,
                    summary: summeryV
                })
        },

        logout: function (then) {
            console.log("logout");
            console.log($http.defaults.headers);
            return httpPost('api/v1/users/logout',
                {}, then
                )
        },


        createNewForum: function (nameV, summeryV) {
            return httpPost('api/v1/forums',
                {
                    name: nameV,
                    summary: summeryV
                })
        },

        addNewThread: function (id,nameV, summeryV) {
            return httpPost('api/v1/subforums/' + id + "/threads",
                {
                    name: nameV,
                    summary: summeryV
                })
        },
        addNewPost: function (id, nameV, summeryV) {
            return httpPost('api/v1/threads/' + id + "/posts",
                {
                    name: nameV,
                    summary: summeryV
                })
        },
        

         getForum: function (id,then) {
                return httpGet('api/v1/forums/' + id, then);
         },

         getThread: function (id, then) {
             return httpGet('api/v1/threads/' + id, then);
         },

         getsubforum: function (id, then) {
             return httpGet('api/v1/subforums/' + id, then);
         },

        getAllSubforums: function (id,then) {
            return httpGet('api/v1/forums/' + id + '/subforums', then);
        },

        getAllThreads: function (id, then) {
            return httpGet('api/v1/subforums/' + id + '/threads', then);
        },

        getAllPosts: function (id, then) {
            return httpGet('api/v1/threads/' + id + '/posts', then);
        },

        createNewSubForum: function (id,nameV, summeryV) {
            return httpPost('api/v1/forums/' + id + '/subforums',
                {
                    name: nameV,
                    summary: summeryV
                })
        }


    }

});




