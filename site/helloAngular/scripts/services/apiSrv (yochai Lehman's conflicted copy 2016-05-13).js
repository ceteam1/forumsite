﻿/// <reference path="C:\Users\jbh\Desktop\14-2-2016\helloAngular\helloAngular\components/angular/angular.js" />
/// <reference path="../models/customerModel.js" />

angular.module("Xtrack").factory("apiSrv", function ($http, $q, $rootScope) {

    var url = "http://localhost:6140/";
    $rootScope.accessToken = "";
    //var MyCustomers = [];
    var d = $q.defer();
    var httpGet = function(apiUrl){
        $http({
            method: "get",
            url: url+apiUrl
        }).then(
                  function (results) {
                      console.log("getting from api " + apiUrl);
                      console.log(results.data);
                      //var forums = JSON.parse(results.data);
                      console.log("data recived..");
                      d.resolve(results.data);
                  },
                  function (err) {
                      console.log(err);
                      d.reject(err);
                  });
        //}
        return d.promise;
    }

    var httpPost = function (apiUrl, dataV) {

        $http({
                method: 'POST',
                url: url + apiUrl,
                data: dataV
            }).then(
                 function (results) {
                     d.notify("posting to api " + url + apiUrl + " data" + dataV);
                     console.log(results.data);
                     //var res = JSON.parse(results.data.content);
                     d.notify("data recived..");
                     d.resolve(results.data);
                 },
                 function (err) {
                     d.reject(err);
                 });
            //}
            return d.promise;
        }

    return {
        getAllForums: function () {
            return httpGet('api/v1/forums');
        },

        register: function (name, password, email) {
            return httpPost("api/v1/users/register",
                {
                    name: name,
                    password: password,
                    email: email
                });
        },


        setAccessToken: function (token) {
            $rootScope.accessToken = token;
        },

        login: function (nameV, passwordV, forum) {
            if (forum != "") {
                forum = "/" + forum
            }
            console.log("login: " +forum);
            var user = httpPost('api/v1/users/login' + forum,
                {
                    name: nameV,
                    password: passwordV
                })
            //$http.defaults.headers.common.user_token = "Sss";
            return user;
        },


        createNewForum: function (nameV, summeryV) {
            return httpPost('api/v1/forums',
                {
                    name: nameV,
                    summary: summeryV
                })
        }


    }

});




