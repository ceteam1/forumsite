﻿/// <reference path="C:\Users\jbh\Desktop\14-2-2016\helloAngular\helloAngular\components/angular/angular.js" />
/// <reference path="../models/customerModel.js" />

angular.module("Xtrack").factory("notificationSrv", function ($http,$q, $rootScope) {

    // We return this object to anything injecting our service
    var Service = {};
    var pending = function (){};
    // Keep all pending requests here until they get responses
    var callbacks = {};
    // Create a unique callback ID to map requests to responses
    var currentCallbackId = 0;
    var ip = "132.73.204.242";
    // Create our websocket object with the address to the websocket
    var ws = undefined; //new WebSocket("");ws://" + ip + ":6140/api/v1/notifications/register");
    var listnerFunc;

    function sendRequest(request) {
        var defer = $q.defer();
        console.log('Sending request', request);
        ws.send(request);
        return defer.promise;
    }

    function listener(data) {

        console.log("Received data from websocket: ", data);
        listnerFunc(data)
        //$rootScope.notify(data);

    }

  
  
    

    return {
        connect: function (token) {
            if (ws == undefined) {
            console.log("opening new socket");


                ws = new WebSocket("ws://" + ip + ":6140/api/v1/notifications/register");
            }
            if (ws.readyState != 1) {
                console.log("22");
                ws.close();
                ws = new WebSocket("ws://" + ip + ":6140/api/v1/notifications/register");

                ws.onopen = function () {
                    console.log("Socket has been opened");
                sendRequest(token);

                }


                ws.onmessage = function (message) {
                    listener(JSON.parse(message.data));
                    console.log("notification:");
                    console.log(message);
                };


               
            } else {
               
                sendRequest(token);
            
            }
    },
        register: function (func) {

            listnerFunc = func;
        },

        logOut: function () {
            ws.close();
        }
}
    

});




