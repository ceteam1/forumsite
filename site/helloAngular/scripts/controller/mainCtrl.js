﻿



var app = angular.module("Xtrack");
app.controller("mainCtrl", function ($window, $http, $scope, $q, apiSrv, notificationSrv, $routeParams)
{
    var _this = this;

    $scope.notification = [];
   
    $scope.menuClass = function (page) {
        console.log(page)
            var current = $location.path().substring(1);
            return page === current ? "active" : "";
    };

    $scope.changeSelectedTab = function (tabName) {
        $('#' + $scope.selectedTab).removeClass('active');
        $('#' + tabName).addClass('active');
        $scope.selectedTab = tabName;
    };

    $scope.notify = function (data) {
        $scope.notification.push(data);
        console.log(data);
        var popover = $('[data-toggle="popover"]').data('bs.popover');
        popover.options.content = getNotificationsList();
      
            $scope.$apply();
    }

    $scope.initMain = function () {
        notificationSrv.register($scope.notify);
          delete $http.defaults.headers.common['X-Requested-With'];
        $('[data-toggle="popover"]').popover({
            content: "",
            html: true
        }
    );
        };
    

    $("[data-toggle='popover']").on('hidden.bs.popover', function () {
        $scope.notification = [];
        $scope.$apply();
    });



    var getNotificationsList = function () {
        var res = "<div class='panel-group'>";
        for (i = 0; i < $scope.notification.length;i++) {
            var data = $scope.notification[i];
            if (data.eventObject != undefined) {
                var event = JSON.parse(data.eventObject);
                console.log(data);
                res += "<div  class='panel panel-default'>";
                res += "new notification: "
                try{
                    res += event.creator.name + " ";
                }catch(e){}
                try{
                res += data.action + " "
                }catch(e){}
                try{
            res += event.type
                }catch(e){}
                try{
        res += " at " + event.parent.type + " "
                }catch(e){}
                try {
                res += event.parent.name;
}catch(e){}
                res += "</div>";
            } else {
                console.log("notification undifined");
                res += "login succesfull"
            }
        }
        return res += "</div>";
    }

    var loginToSite = function (res){
        if (res.accessToken != -1) {
            $scope.loggedIn = true;
            $scope.loggedUser = res.content
            $http.defaults.headers.common.user_token = res.accessToken;
            $scope.sessionID = res.accessToken;
            console.log("notificationSrv");
            notificationSrv.connect(res.accessToken);
            console.log("notificationSrv22");
            $scope.notification.pop();
            $scope.notification.pop();
        } else {
            var accsessNum = prompt("you are already connected from anouter devicr. \n enter user token: ", "your token here");
            if (accsessNum != null) {
                console.log("accsessNum:" + accsessNum)
                $http.defaults.headers.common.user_token = accsessNum;
                apiSrv.login($('#userName').val(), $('#userPassword').val(), $routeParams.forumID, loginToSite);
            }
        }

    }



    $scope.login = function () {
        apiSrv.login($('#userName').val(), $('#userPassword').val(), $routeParams.forumID, loginToSite);
    }

    $scope.logOut = function () {
        apiSrv.logout(function () {
            $scope.loggedIn = false;
            $scope.loggedUser = undefined
            $http.defaults.headers.common.user_token = undefined;
            notificationSrv.logOut();
        });

    }

    $scope.saveNewForum = function () {
        apiSrv.createNewForum($scope.newForumName, $scope.newForumSummery);

    }





});

