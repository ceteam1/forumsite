﻿var app = angular.module("Xtrack");
app.controller("subforumCtrl", function ($scope, $window, apiSrv, $route, $routeParams) {


    $scope.initForums = function () {
        console.log("initForums");
        apiSrv.getAllForums(function (res) {
            //console.log(res.content);
            $scope.forums = res.content;
        });
    };

    $scope.initForum = function () {
        $scope.NewSubForum = false;
        apiSrv.getforum($routeParams.forumID, function (res) {
            $scope.forum = res.content;
        });
        console.log("init Forum " + $routeParams.forumID);
        apiSrv.getAllSubforums($routeParams.forumID, function (res) {
            $scope.subforums = res.content;
        });
    };


    $scope.initSubForum = function () {
        $scope.NewThread = false;
        apiSrv.getsubforum($routeParams.subforumID, function (res) {
            $scope.subforum = res.content;
        });

        console.log("init subforum " + $routeParams.subforumID);
        apiSrv.getAllThreads($routeParams.subforumID, function (res) {
            $scope.threads = res.content;
            console.log(res.content);
        });
    };


    $scope.initThread = function () {
        $scope.NewPost = false;
        apiSrv.getThread($routeParams.threadID, function (res) {
            $scope.thread = res.content;
            console.log($scope);
            console.log(res.content);
        });

        console.log("init thread " + $routeParams.threadID);
        apiSrv.getAllPosts($routeParams.threadID, function (res) {
            $scope.posts = res.content;
           
        });
    };


    $scope.register = function () {
        apiSrv.register($scope.name,$scope.password,$scope.email)
    }

    $scope.addNewForum = function () {
        apiSrv.createNewForum($('#newForumName').val(), $('#newForumSummery').val())
        $route.reload();
    }


    $scope.addNewSubForum = function () {
        apiSrv.createNewSubForum($routeParams.forumID,$('#newSubForumName').val(), $('#newSubForumSummery').val())
        $route.reload();
    }

    $scope.createNewThread = function () {
        console.log(apiSrv.addNewThread($routeParams.subforumID, $('#newThreadName').val(), $('#newThreadSummery').val()))
        $route.reload();
    }


    $scope.createNewPost = function () {
        console.log(apiSrv.addNewPost($routeParams.threadID, $('#newostName').val(), $('#newPostSummery').val()))
        $route.reload();
    }



    


});