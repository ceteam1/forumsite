﻿var app = angular.module("Xtrack");
app.controller("allforumsCtrl", function ($scope, $window, notificationSrv, apiSrv, $route, $routeParams) {


    $scope.initAllForums = function () {
        console.log("initForums");
        apiSrv.getAllForums(function (res) {
            //console.log(res.content);
            $scope.forums = res.content;
        });
    };


    $scope.register = function () {
        apiSrv.register($scope.name,$scope.password,$scope.email)
    }

    $scope.addNewForum = function () {
        apiSrv.createNewForum($('#newForumName').val(), $('#newForumSummery').val())
        $route.reload();
    }



});