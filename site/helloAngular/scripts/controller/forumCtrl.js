﻿var app = angular.module("Xtrack");
app.controller("forumCtrl", function ($scope, $window, apiSrv, $route, $routeParams) {



    $scope.initForum = function () {
        apiSrv.getForum($routeParams.forumID, function (res) {
            $scope.forum = res.content;
        });
        console.log("init Forum " + $routeParams.forumID);
        console.log($routeParams.forumID);
        apiSrv.getAllSubforums($routeParams.forumID, function (res) {
            $scope.subforums = res.content;
        });
    };


    $scope.addNewSubForum = function () {
        apiSrv.createNewSubForum($routeParams.forumID,$('#newSubForumName').val(), $('#newSubForumSummery').val())
        $route.reload();
    }



    


});