﻿// testing controller
describe('allforums', function () {

    beforeEach(angular.mock.module('Xtrack'));

    var $controller;

    beforeEach(angular.mock.inject(function(_$controller_){
        $controller = _$controller_;
    }));





        $httpBackend.whenRoute('GET', 'forums/:id')
  .respond(function (method, url, data, headers, params) {
      return [200, MockForumList[Number(params.id)]];
  });

        $httpBackend.whenRoute('GET', 'subforums/:id')
.respond(function (method, url, data, headers, params) {
    return [200, MockForumList[Number(params.id)]];
});

        $httpBackend.whenRoute('GET', 'threads/:id')
.respond(function (method, url, data, headers, params) {
    return [200, MockForumList[Number(params.id)]];
});

  
        $httpBackend.whenRoute('GET', '/forums')
          .respond(function (method, url, data, headers, params) {
              return [200, {
                  content: MockForumList
              }];
          });

        $httpBackend.whenRoute('GET', '/forums')
     .respond(function (method, url, data, headers, params) {
         return [200, {
             content: MockForumList
         }];
     });

        $httpBackend.whenRoute('GET', 'forums/:id/subforums')
     .respond(function (method, url, data, headers, params) {
        return [200, {
            content: MockSubforumList[Number(params.id)]
         }];
     });

        $httpBackend.whenRoute('GET', 'subforums/:id/threads')
     .respond(function (method, url, data, headers, params) {
         return [200, {
             content: MockThreadList[Number(params.id)]
         }];
     });

        $httpBackend.whenRoute('GET', 'subforums/:id/threads')
  .respond(function (method, url, data, headers, params) {
      return [200, {
          content: MockPostList[Number(params.id)]
      }];
  });







        $httpBackend.whenRoute('POST', 'users/login')
.respond(function (method, url, data, headers, params) {
    return [200, {
        content: MockUsersList[data.userid]
    }];
});


        $httpBackend.whenRoute('POST', 'forums')
.respond(function (method, url, data, headers, params) {
    var idV = MockForumsList.length + 1;
         MockForumsList.put( {
             id: idV,
        name: data.name,
        summary: data.summery
    }) 
    return [200, {
        content: MockForumsList[idV]
    }];
});

        
        $httpBackend.whenRoute('POST', 'subforums/:id/threads')
.respond(function (method, url, data, headers, params) {
    var idV = MockThreadsList.length + 1;
    MockThreadsList.put( {
        id: idV,
        name: data.name,
        summary: data.summery
    }) 
    return [200, {
        content: MockThreadsList[Number(params.id)[idV]]
    }];
});

        $httpBackend.whenRoute('POST', 'threads/:id/posts')
.respond(function (method, url, data, headers, params) {
    var idV = MockPostsList.length + 1;
    MockPostsList.put( {
        id: idV,
        name: data.name,
        summary: data.summery
    }) 
    return [200, {
        content: MockPostsList[Number(params.id)[idV]]
    }];
});

        $httpBackend.whenRoute('POST', 'forums/:id/subforums')
.respond(function (method, url, data, headers, params) {
    var idV = MockSubforumsList.length + 1;
    MockSubforumsList.put( {
        id: idV,
        name: data.name,
        summary: data.summery
    }) 
    return [200, {
        content: MockSubforumsList[Number(params.id)[idV]]
    }];
});




        // Get hold of a scope (i.e. the root scope)
        $rootScope = $injector.get('$rootScope');
        // The $controller service is used to create instances of controllers
        var $controller = $injector.get('$controller');

        createController = function () {
            return $controller('MyController', { '$scope': $rootScope });
        };
    }));


    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });



////tests
    describe('nouser login should fail', function () {
        it('nouser login should fail', function () {
            var $scope = {};
            var controller = $controller('mainCtrl', { $scope: $scope });
            expect($scope.loggedIn).toBe(false);
            expect($scope.loggedUser).toBe(undefined);
            expect($http.defaults.headers.common.user_token).toBe(undefined);
            $httpBackend.flush();
        });
    });


});