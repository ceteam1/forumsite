﻿angular.module("XtrackFilters",[]).filter('currentEventFilter', function () {
    return function (input, type) {
        var output = [];
        if (type === 'All' || type === '' || type === undefined) {
            return input;
          
        }
        angular.forEach(input, function (c) {
            if (c.Requesttype === type) {
                output.push(c);
            }
        })
        return output;
    }
});
