﻿/// <reference path="C:\Users\jbh\Desktop\14-2-2016\helloAngular\helloAngular\components/angular/angular-route.js" />
/// <reference path="C:\Users\jbh\Desktop\14-2-2016\helloAngular\helloAngular\components/angular/angular.js" />


// we move into the "ControllerAs" Patern
// and practice this patern with demo1, demo2 and demo3
// as for demo6 we must use "ControllerAs" as we want to
// force isolated storage for $scope.editRow as the ng-repeat
// generates $scope for each line...
// In general use the "controllerAs"  patern by default !!!


angular.module("Xtrack", ["ngRoute"])
    .config(function ($routeProvider)
    {
        $routeProvider
        .when("/allForums",
            {
                templateUrl: "views/allForums.html",
                controller: "allforumsCtrl"
            })
            .when("/forums/:forumID",
            {
                templateUrl: "views/forum.html",
                controller: "forumCtrl"
            })
            .when("/subforum/:subforumID",
            {
                templateUrl: "views/subforum.html",
                controller: "subforumCtrl"
            })
           .when("/thread/:threadID",
           {
               templateUrl: "views/thread.html",
               controller: "threadCtrl"
           })
            .when("/register",
           {
               templateUrl: "views/tests.html",
               controller: "mainCtrl"
           })
              .when("/tests",
           {
               templateUrl: "views/tests.html",
           })
            .otherwise(
           {
               templateUrl: "views/allForums.html",
               controller: "allforumsCtrl"
           })


    }
);